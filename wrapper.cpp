#include "wrapper.h"

MyWrapper::MyWrapper(GMlib::ArrayLX<Node*> nodes)
{
    this->__nodes = nodes;
    for(int i = 0; i<nodes.getSize(); i++){
        std::cout << "node " << i << " has " << nodes(i)->getTriangles().getSize() << " triangles" << std::endl;

    }
}

void
MyWrapper::setPressure(double newPressure)
{
    this->__pressure = newPressure;
}

double
MyWrapper::getPressure()
{
    return this->__pressure;
}

void
MyWrapper::calculateStiffnessMatrix()
{
    std::cout << "creating stiffness matrix ... node: " << this->__nodes.getSize() << std::endl;

    std::cout << "computing values outside diagonal ..." << std::endl;
    //computing values outside diagonal
    //iterating rows
    double vals[this->__nodes.getSize()*this->__nodes.getSize()];
    for(int i = 0; i < this->__nodes.getSize(); i++){

        //iterating columns
        for(int j = i + 1; j < this->__nodes.getSize(); j++){
            std::cout << "computing node " << i << " against node " << j <<  std::endl;
            vals[i * this->__nodes.getSize() + j] = (this->__nodes(i)->calculateStiffnessAgainstOtherNode(this->__nodes(j)));
            vals[j * this->__nodes.getSize() + i] = vals[i * this->__nodes.getSize() + j];
        }
    }

    std::cout << "computing values inside diagonal ..." << std::endl;
    //computing matrix diagonal
    for(int i = 0; i < this->__nodes.getSize(); i++){
        vals[i * this->__nodes.getSize() + i] = (this->__nodes(i)->calculateStiffnessAgainstSelf());
    }
    this->__stiffnessMatrix = GMlib::DMatrix<double>(this->__nodes.getSize(),this->__nodes.getSize(), vals);

}

void
MyWrapper::calculateLoadVector()
{
    double vals[this->__nodes.getSize()];
    for(int i=0; i < this->__nodes.getSize(); i++){
        vals[i] = this->__nodes[i]->calculateLoad() * this->__pressure;
    }
    this->__loadVector = GMlib::DVector<double>(this->__nodes.getSize(),vals);
}

void
MyWrapper::printStiffnessMatrix()
{
    for(int i = 0; i < this->__nodes.getSize(); i++){
        for(int j = 0; j < this->__nodes.getSize(); j++){
            std::cout << this->__stiffnessMatrix[  i ][ j ] << "\t";
        }
        std::cout << std::endl;
    }

}


GMlib::DVector<double>
MyWrapper::calculateNewValues()
{
    GMlib::DMatrix<double> tempstiffnessMatrix = this->__stiffnessMatrix;
    tempstiffnessMatrix.invert();
    GMlib::DVector<double> newvals = tempstiffnessMatrix * this->__loadVector;
    return newvals;
}
