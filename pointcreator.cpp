#include "pointcreator.h"

#define _USE_MATH_DEFINES

#include <math.h>

Mesh::Mesh(float radius, float offsetX, float offsetY) : TriangleFacets<float>()
{
    this->_radius = radius;
    this->_offsetX = offsetX;
    this->_offsetY = offsetY;
}

Mesh::~Mesh(){}


void
Mesh::createPoints(){}


void
Mesh::localSimulate(double dt)
{
    if(!this->_locked){
        this->_locked = true;
        if(this->_pressure > 0.3 || this->_pressure<-0.3) this->_dir=this->_dir * (-1);
        this->_pressure = this->_pressure + this->_dir*0.01;
        this->_wp->setPressure(this->_pressure);
        std::cout << "pressure is " << this->_pressure << std::endl;
        this->_wp->calculateLoadVector();
        GMlib::DVector<double> newvals = this->_wp->calculateNewValues();
        int t = 0;
        for(int i = 0; i < this->getNoVertices(); i++){
            if(!this->getVertex(i)->boundary()){
                //std::cout << "vertex " << t << " gets Z-value " << newvals[t]<< std::endl;
                this->getVertex(i)->setZ(newvals[t]);
                t++;
            }
        }
        this->replot();
        this->_locked = false;
    }
}

void
Mesh::createElements()
{
    GMlib::ArrayLX<Node*> nodes;
    GMlib::ArrayLX<GMlib::TSTriangle<float>* > triangles;
    for(int i =0; i < this->getNoTriangles(); i++){
        triangles.insertAlways(this->getTriangle(i));
    }
    Node initNode(triangles, this->_centerPoint);
    for(int i = 0; i < this->getNoVertices(); i++){
        if(!this->getVertex(i)->boundary()){
           GMlib::ArrayLX<GMlib::TSTriangle<float>* > tempTriangles = initNode.findTrianglesWithPoint(this->getVertex(i));
           nodes.insertAlways(new Node(tempTriangles, *this->getVertex(i)));
        }
    }
    std::cout << "node objects: " << nodes.getSize() << std::endl;

    this->_wp = new MyWrapper(nodes);
    this->_wp->calculateStiffnessMatrix();
    this->_wp->printStiffnessMatrix();
    this->_dir = 1;
    this->_pressure = 0.3;
    this->replot();

}

RandomPoints::RandomPoints(unsigned int numberOfTriangles, float radius, float offsetX, float offsetY) : Mesh(radius, offsetX, offsetY)
{
    this->__numberOfTriangles = numberOfTriangles;
}

unsigned int
RandomPoints::getNumberOfPointsOnBoundary()
{
    return (int) ((2.0 * M_PI * this->_radius) / ( floor(std::sqrt((4.0 * M_PI * (float) std::pow((float) this->_radius,2.0))/((float) std::sqrt(3.0)* (float) this->__numberOfTriangles)))));
}

unsigned int
RandomPoints::getNumberOfPoints()
{
    return floor((float) this->__numberOfTriangles + 2.0 + (float) this->getNumberOfPointsOnBoundary())/2.0;
}

void
RandomPoints::createPoints()
{
    this->clear();
    // insert center point
    this->_centerPoint = GMlib::TSVertex<float>(GMlib::Point<float,2>({this->_offsetX,this->_offsetY}));
    this->insertAlways(this->_centerPoint);



    GMlib::Angle ang = GMlib::Angle((float) (2.0*M_PI)/this->getNumberOfPointsOnBoundary());
    GMlib::Vector<float, 2> u = GMlib::Vector<float, 2>({0,1});
    GMlib::Vector<float, 2> v = GMlib::Vector<float, 2>({1,0});
    GMlib::SqMatrix<float, 2> rotMatrix(ang, u, v);

    GMlib::Point<float, 2> np = GMlib::Point<float,2>({0.0, this->_radius});
    GMlib::TSVertex<float> tv = GMlib::TSVertex<float>(np);

    for(int t = 0; t < this->getNumberOfPointsOnBoundary(); t++){
        tv = tv + this->_centerPoint;
        this->insertAlways(tv);
        tv = tv - this->_centerPoint;
        np = rotMatrix * tv;
        tv = GMlib::TSVertex<float>(np);
    }

    this->createRandomPoints();
}


void
RandomPoints::createRandomPoints()
{
    float s = std::sqrt(4.0*M_PI*this->_radius/(std::sqrt(3.0)*this->__numberOfTriangles));
    if(this->getSize() < this->getNumberOfPoints())
    {

        float x = (rand() % (int) (this->_radius*2)) + this->_offsetX - this->_radius;
        float y = (rand() % (int) (this->_radius*2)) + this->_offsetY - this->_radius;
        GMlib::TSVertex<float> npv = GMlib::TSVertex<float>(GMlib::Point<float,2>({x,y}));
        bool isOK = true;

        if(fabs((npv.getPos() - this->_centerPoint.getPos()).getLength()) > this->_radius){
            isOK = false;
        }

        for(int i = 0; i< this->getNoVertices(); i++){
            if(fabs((this->getVertex(i)->getPos() - npv.getPos()).getLength()) < (s/2))
                isOK = false;
        }
        if(isOK)  this->insertAlways(npv);
        this->createRandomPoints();
    }

}




RegularPoints::RegularPoints(unsigned int numberOfCircles, unsigned int numberOfPointsInnerCircle, float radius, float offsetX, float offsetY) : Mesh(radius, offsetX, offsetY)
{
    this->__numberOfCircles = numberOfCircles;
    this->__numberOfPointsInnerCircle = numberOfPointsInnerCircle;
}

void
RegularPoints::createPoints()
{
    this->clear();
    std::cout << "number of circles: " << this->__numberOfCircles << std::endl;
    std::cout << "pointsic: " << this->__numberOfPointsInnerCircle << std::endl;

    // insert center point
    this->_centerPoint = GMlib::TSVertex<float>(GMlib::Point<float,2>({this->_offsetX,this->_offsetY}));
    this->insertAlways(this->_centerPoint);


    for(int i = 0; i < this->__numberOfCircles; i++)
    {
        unsigned int pointsOnCircle = std::pow(2,i) * this->__numberOfPointsInnerCircle;
        std::cout << "circle " << i << " has " << pointsOnCircle << " points" << std::endl;

        GMlib::Angle ang = GMlib::Angle((float) (2.0*M_PI)/pointsOnCircle);
        GMlib::Vector<float, 2> u = GMlib::Vector<float, 2>({0,1});
        GMlib::Vector<float, 2> v = GMlib::Vector<float, 2>({1,0});
        GMlib::SqMatrix<float, 2> rotMatrix(ang, u, v);
        float radiusc = (i+1) * (this->_radius / (float) this->__numberOfCircles);
        std::cout << "circle " << i << " has radius " << radiusc << std::endl;

        GMlib::Point<float, 2> np = GMlib::Point<float,2>({0.0, radiusc});
        GMlib::TSVertex<float> tv = GMlib::TSVertex<float>(np);

        for(int t = 0; t < pointsOnCircle; t++){
            tv = tv + this->_centerPoint;
            this->insertAlways(tv);
            tv = tv - this->_centerPoint;
            np = rotMatrix * tv;
            tv = GMlib::TSVertex<float>(np);
        }
    }
}
