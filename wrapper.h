#ifndef WRAPPER_H
#define WRAPPER_H

#include <core/gmglobal>
#include <trianglesystem/gmtrianglesystem.h>

#include "node.h"

class MyWrapper
{
public:
                                MyWrapper(GMlib::ArrayLX<Node*> nodes);
    void                        calculateStiffnessMatrix();
    void                        calculateLoadVector();
    GMlib::DVector<double>      calculateNewValues();
    void                        printStiffnessMatrix();
    void                        setPressure(double newPressure);
    double                      getPressure();


private:
    GMlib::DMatrix<double>      __stiffnessMatrix;
    GMlib::DVector<double>      __loadVector;
    GMlib::ArrayLX<Node*>       __nodes;
    double                      __pressure;
};

#endif // WRAPPER_H
