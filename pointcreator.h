#ifndef POINTCREATOR_H
#define POINTCREATOR_H

#include <core/gmglobal.h>
#include <trianglesystem/gmtrianglesystem.h>

#include "node.h"
#include "wrapper.h"

class Mesh : public GMlib::TriangleFacets<float>
{
public:
    Mesh(float radius, float offsetX, float offsetY);
    ~Mesh();
    void createPoints();
    void createElements();


protected:
    GMlib::TSVertex<float>  _centerPoint;
    float                   _radius;
    float                   _offsetX;
    float                   _offsetY;
    MyWrapper               *_wp;
    double                  _pressure;
    int                     _dir;
    void                    localSimulate(double dt);

};




class RandomPoints : public Mesh {
public:
                            RandomPoints(unsigned int numberOfTriangles, float radius, float offsetX, float offsetY);
    void                    createPoints();
    void                    createRandomPoints();
    unsigned int            getNumberOfPointsOnBoundary();
    unsigned int            getNumberOfPoints();


private:
    unsigned int            __numberOfTriangles;

};


class RegularPoints : public Mesh{

public:
                            RegularPoints(unsigned int numberOfCircles, unsigned int numberOfPointsInnerCircle, float radius, float offsetX, float offsetY);
    void                    createPoints();

private:
    unsigned int            __numberOfCircles;
    unsigned int            __numberOfPointsInnerCircle;

};


#endif // POINTCREATOR_H
