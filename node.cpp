#include "node.h"

#include <core/gmarray>
Node::Node(){}

Node::Node(GMlib::ArrayLX<GMlib::TSTriangle<float>*> triangles, GMlib::TSVertex<float> centerPoint)
{
    this->triangles = triangles;
    this->centerPoint = centerPoint;
}


GMlib::ArrayLX<GMlib::TSTriangle<float> *>
Node::findTrianglesWithPoint(GMlib::TSVertex<float> *p)
{
    GMlib::ArrayLX<GMlib::TSTriangle<float> *> result;
    for(int i = 0; i < this->triangles.getSize(); i++){
        GMlib::Array< GMlib::TSVertex< float > * > vertices = this->triangles(i)->getVertices();
        for(int t = 0; t < vertices.getSize(); t++){
            if(vertices(t)->operator ==(*p)) result.insertAlways(this->triangles(i));
        }
    }
    return result;
}

GMlib::ArrayLX<GMlib::TSTriangle<float>* >
Node::findTrianglesWithEdge(GMlib::TSEdge<float> *p)
{
    GMlib::ArrayLX<GMlib::TSTriangle<float>* > result;
    for(int i = 0; i < this->triangles.getSize(); i++){
        GMlib::Array< GMlib::TSEdge< float > * > edges = this->triangles(i)->getEdges();
        for(int t = 0; t < edges.getSize(); t++){
            if(this->isSameEdge(edges(t), p)) result.insertAlways(this->triangles(i));
        }
    }
    return result;
}
double
Node::calculateStiffnessAgainstSelf()
{
    double sum = 0.0;
    for(int i = 0; i < this->triangles.getSize(); i++){
        sum += (this->calculateStiffnessSameTriangle(this->triangles(i)));
    }
    return sum;
}


double
Node::calculateStiffnessAgainstOtherNode(Node *n)
{
    double sum = 0.0;
    GMlib::ArrayLX< GMlib::TSTriangle<float>* > trs = n->getTriangles();
    GMlib::ArrayLX< GMlib::TSTriangle<float>* > sametrs;
    for(int i = 0; i < this->triangles.getSize(); i++){
        for(int j = 0; j < trs.getSize(); j++){
            if(this->isSameTriangle(this->triangles(i),  trs(j))){
                sametrs.insertAlways(this->triangles(i));
            }
        }
    }
    if(sametrs.getSize()>0){
        sum =  this->calculateStiffnessDifferentTriangles(sametrs(0), sametrs(1));
    }
    return sum;
}

GMlib::ArrayLX< GMlib::TSTriangle<float>* >
Node::getTriangles()
{
    return this->triangles;
}


bool
Node::isSameEdge(GMlib::TSEdge<float> *e1, GMlib::TSEdge<float> *e2)
{
    return ((e1->getFirstVertex() == e2->getFirstVertex() && e1->getLastVertex() == e2->getLastVertex()) || (e1->getFirstVertex() == e2->getLastVertex() && e1->getLastVertex() == e2->getFirstVertex()));
}

GMlib::TSEdge< float >*
Node::getCommonEdge(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2)
{
    GMlib::Array< GMlib::TSEdge< float > * > es1 = t1->getEdges();
    GMlib::Array< GMlib::TSEdge< float > * > es2 = t2->getEdges();
    for(int i = 0; i < 3; i++){
        for(int t = 0; t < 3; t++){
            if(this->isSameEdge(es1[i], es2[t]))
                return es1[i];
        }
    }
    return new GMlib::TSEdge< float >();
}

double
Node::calculateStiffnessDifferentTriangles(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2)
{
    GMlib::TSEdge< float > *ce = this->getCommonEdge(t1 ,t2);
    if(ce->getFirstVertex() == NULL || ce->getLength()==0.0) return 0.0;
    GMlib::Vector<float, 2> d = ce->getVector2D();
    GMlib::Vector<float, 2> a1;
    GMlib::Vector<float, 2> a2;
    GMlib::Array< GMlib::TSVertex< float > * > vs1 = t1->getVertices();
    GMlib::Array< GMlib::TSVertex< float > * > vs2 = t2->getVertices();

    for(int i = 0; i < 3; i++){
        if(!vs1[i]->operator ==(*ce->getFirstVertex()) && !vs1[i]->operator ==(*ce->getLastVertex())){
            a1 = vs1[i]->getArrow() - ce->getFirstVertex()->getArrow();
        }
        if(!vs2[i]->operator ==(*ce->getFirstVertex()) && !vs2[i]->operator ==(*ce->getLastVertex())){
            a2 = vs2[i]->getArrow() - ce->getFirstVertex()->getArrow();
        }
    }
    double dd = 1.0 / (d[0]*d[0] + d[1]*d[1]);

    double area1 = fabs(d[0]*a1[1] - d[1]*a1[0]);
    double dh1 = (dd * (a1[0]*d[0] + a1[1]*d[1]));
    double h1 = (dd * area1 * area1);

    double area2 = fabs(a2[0]*d[1] - a2[1]*d[0]);
    double dh2 = (dd * (a2[0]*d[0] + a2[1]*d[1]));
    double h2 = (dd * area2 * area2);
    double res = ((dh1*(1.0-dh1)/h1 - dd) * (area1/2.0) + (dh2*(1.0-dh2)/h2 - dd) * (area2/2.0));
    return res;
}

double
Node::calculateStiffnessSameTriangle(GMlib::TSTriangle<float> *t1)
{
    GMlib::Array< GMlib::TSEdge< float > * > es = t1->getEdges();

    GMlib::Vector<float, 2> d1,d2,d3;
    if(!es[0]->getFirstVertex()->operator ==(this->centerPoint) && !es[0]->getLastVertex()->operator ==(this->centerPoint)){
        if(es[1]->getFirstVertex()->operator ==(this->centerPoint)) d1 = es[1]->getVector2D(); else d1 = es[1]->getVector2D().operator *(-1.0);
        if(es[2]->getFirstVertex()->operator ==(this->centerPoint)) d2 = es[2]->getVector2D(); else d2 = es[2]->getVector2D().operator *(-1.0);
        d3 = es[0]->getVector2D();
    }
    if(!es[1]->getFirstVertex()->operator ==(this->centerPoint) && !es[1]->getLastVertex()->operator ==(this->centerPoint)){
        if(es[0]->getFirstVertex()->operator ==(this->centerPoint)) d1 = es[0]->getVector2D(); else d1 = es[0]->getVector2D().operator *(-1.0);
        if(es[2]->getFirstVertex()->operator ==(this->centerPoint)) d2 = es[2]->getVector2D(); else d2 = es[2]->getVector2D().operator *(-1.0);
        d3 = es[1]->getVector2D();
    }
    if(!es[2]->getFirstVertex()->operator ==(this->centerPoint) && !es[2]->getLastVertex()->operator ==(this->centerPoint)){
        if(es[0]->getFirstVertex()->operator ==(this->centerPoint)) d1 = es[0]->getVector2D(); else d1 = es[0]->getVector2D().operator *(-1.0);
        if(es[1]->getFirstVertex()->operator ==(this->centerPoint)) d2 = es[1]->getVector2D(); else d2 = es[1]->getVector2D().operator *(-1.0);
        d3 = es[2]->getVector2D();

     }
    double res = (d3[0]*d3[0]+d3[1]*d3[1])/(2.0 *  fabs(d1[0]*d2[1] - d1[1]*d2[0]));

    return res;
}

bool
Node::isSameTriangle(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2)
{
    GMlib::Array< GMlib::TSVertex< float > * > vs1 = t1->getVertices();
    GMlib::Array< GMlib::TSVertex< float > * > vs2 = t2->getVertices();
    for(int i = 0; i < 3; i++){
        bool f = false;
        for(int t = 0; t < 3; t++){
            if(vs1[i]->operator ==(*vs2[t])) f = true;
        }
        if(!f){
            return false;
        }
    }
    return true;
}

double
Node::calculateLoad()
{
    double res = 0.0;
    for(int i = 0; i < this->triangles.getSize(); i++)
    {
        GMlib::Vector<float, 2> d1,d2;
        if(!this->triangles[i]->getEdges()[0]->getFirstVertex()->operator ==(this->centerPoint) && !this->triangles[i]->getEdges()[0]->getLastVertex()->operator ==(this->centerPoint)){
           d1 = this->triangles[i]->getEdges()[1]->getVector2D();
           d2 = this->triangles[i]->getEdges()[2]->getVector2D();
        }
        if(!this->triangles[i]->getEdges()[1]->getFirstVertex()->operator ==(this->centerPoint) && !this->triangles[i]->getEdges()[1]->getLastVertex()->operator ==(this->centerPoint)){
            d1 = this->triangles[i]->getEdges()[0]->getVector2D();
            d2 = this->triangles[i]->getEdges()[2]->getVector2D();
        }
        if(!this->triangles[i]->getEdges()[2]->getFirstVertex()->operator ==(this->centerPoint) && !this->triangles[i]->getEdges()[2]->getLastVertex()->operator ==(this->centerPoint)){
           d1 = this->triangles[i]->getEdges()[1]->getVector2D();
           d2 = this->triangles[i]->getEdges()[0]->getVector2D();
        }
        res += fabs(d1[0]*d2[1] - d1[1]*d2[0]) / 6.0;
    }
    return res;
}
