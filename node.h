#ifndef NODE_H
#define NODE_H

#include <core/gmglobal>
#include <core/gmarray>
#include <trianglesystem/gmtrianglesystem.h>

class Node
{
public:
                                                Node();
                                                Node(GMlib::ArrayLX<GMlib::TSTriangle<float>*> triangles, GMlib::TSVertex<float> centerPoint);
    GMlib::ArrayLX<GMlib::TSTriangle<float>* >  findTrianglesWithPoint(GMlib::TSVertex<float> *p);
    GMlib::ArrayLX<GMlib::TSTriangle<float>* >  findTrianglesWithEdge(GMlib::TSEdge<float> *p);
    GMlib::TSEdge< float >*                     getCommonEdge(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2);
    double                                       calculateStiffnessAgainstOtherNode(Node *n);
    double                                       calculateStiffnessAgainstSelf();
    double                                       calculateStiffnessSameTriangle(GMlib::TSTriangle<float> *t1);
    double                                       calculateStiffnessDifferentTriangles(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2);
    double                                       calculateLoad();
    GMlib::ArrayLX< GMlib::TSTriangle<float>* > getTriangles();

private:
    GMlib::ArrayLX< GMlib::TSTriangle<float>* > triangles;
    GMlib::TSVertex<float>                      centerPoint;

    bool                                        isSameTriangle(GMlib::TSTriangle<float> *t1, GMlib::TSTriangle<float> *t2);
    bool                                        isSameEdge(GMlib::TSEdge<float> *e1, GMlib::TSEdge<float> *e2);
};

#endif // NODE_H
